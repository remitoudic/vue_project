new  Vue({
  el:'#app',
  data:{ title:'becomming a Vue Ninja',
          name:  'Rémi',
          url:  'youtube', 
          wage: 10,
          coords: {
            x: 0, 
            y:0,
                 },
          ShowName: true,
          ShowAge: false,
          items:["remi", "paul", "jack", "iwona"],
          prog:[{name: "remi", age: 37},{name: "Paul", age: 30},{name: "Max", age: 35},],

        },
  methods: {
    toogleName(){ this.ShowName = !this.ShowName},
    toogleAge(){ this.ShowAge = !this.ShowAge},
    greet(time) { return  `hello and good  ${time}, ${this.name}`},
    ChangeWage(amount){ this.wage += amount},
    LogEvent(e){console.log(e)}, 
    LogCoords(e){
      this.coords.x= e.offsetX
      this.coords.y= e.offsetY
    },
    UpdateName(e){//console.log(e.target.value)
      this.name=e.target.value
    },
    LogMessage(){console.log('hello')}
   
  }
}
)
