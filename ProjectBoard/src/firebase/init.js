import * as firebase from 'firebase/app';
import 'firebase/firestore';



var firebaseConfig = {
  apiKey: "AIzaSyApy6IOb6fgSvtgGJlrfCztlOfTjeameSA",
  authDomain: "project-board-d5840.firebaseapp.com",
  databaseURL: "https://project-board-d5840.firebaseio.com",
  projectId: "project-board-d5840",
  storageBucket: "project-board-d5840.appspot.com",
  messagingSenderId: "292453082253",
  appId: "1:292453082253:web:253da43682769e8d146ec4",
  measurementId: "G-21VFT83ZKQ"
};
// Initialize Firebase

// Initialize Firebase
// Initialize Firebase
const firebaseApp = firebase.initializeApp(firebaseConfig);
export default firebaseApp.firestore();