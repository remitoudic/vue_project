import  firebase from 'firebase/app';
import 'firebase/firestore';

const firebaseConfig = {
    apiKey: "AIzaSyDJR7utC2sD_G19c5ebck2wFCwTUsFMstk",
    authDomain: "geo-remus007-board.firebaseapp.com",
    databaseURL: "https://geo-remus007-board.firebaseio.com",
    projectId: "geo-remus007-board",
    storageBucket: "geo-remus007-board.appspot.com",
    messagingSenderId: "941426347002",
    appId: "1:941426347002:web:5d2cfb8daa59a57ba2b243",
    measurementId: "G-WNGBTDC1WQ"
  };




// Initialize Firebase
const firebaseApp = firebase.initializeApp(firebaseConfig);
export default firebaseApp.firestore();