import * as firebase from 'firebase/app';
import 'firebase/firestore';

const firebaseConfig = {
    apiKey: "AIzaSyA4l3WHK-MRZI7qQeXEDv-D3saDIMZ8Fwk",
    authDomain: "remus-chatboard.firebaseapp.com",
    databaseURL: "https://remus-chatboard.firebaseio.com",
    projectId: "remus-chatboard",
    storageBucket: "remus-chatboard.appspot.com",
    messagingSenderId: "445548474037",
    appId: "1:445548474037:web:c78c9e3193d79ccb2258fc",
    measurementId: "G-P8DCYEL569"
  };


// Initialize Firebase

// Initialize Firebase
// Initialize Firebase
const firebaseApp = firebase.initializeApp(firebaseConfig);
export default firebaseApp.firestore();